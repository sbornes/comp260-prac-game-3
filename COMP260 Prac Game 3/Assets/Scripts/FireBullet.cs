﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;
	public float reloadTime = 2.5f;
	private bool reloading = false;
	private float reloadtime;

	void Update () {
		if (Time.timeScale == 0) {
			return;
		}
		// when the button is pushed, fire a bullet
		if (Input.GetButtonDown("Fire1")) {
			if (!reloading) {
				BulletMove bullet = Instantiate (bulletPrefab);
				// the bullet starts at the player's position
				bullet.transform.position = transform.position;

				// create a ray towards the mouse location
				Ray ray = 
					Camera.main.ScreenPointToRay (Input.mousePosition);
				bullet.direction = ray.direction;

				reloading = true;

				reloadtime = Time.time + reloadTime;

				//Debug.Log ("reload time is " + reloadtime);
			}
		}

		if (reloading && Time.time >= reloadtime) {
			reloading = false;

		}
	}
}
