﻿using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour {

	// Use this for initialization
	public float speed = 10.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;
	public float range = 25.0f;

	float distanceTravelled = 0;
	Vector3 lastPosition;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();    
	}

	void FixedUpdate () {
		rigidbody.velocity = speed * direction;
	}

	void OnCollisionEnter(Collision collision) {
		// Destroy the bullet
		Destroy(gameObject);
	}
	// Update is called once per frame
	void Update () {
		distanceTravelled += Vector3.Distance(transform.position, lastPosition);
		lastPosition = transform.position;

		if (distanceTravelled >= range) {
			Destroy (gameObject);
		}
	}
}
